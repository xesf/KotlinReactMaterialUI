@file:Suppress("INTERFACE_WITH_SUPERCLASS", "OVERRIDING_FINAL_MEMBER", "RETURN_TYPE_MISMATCH_ON_OVERRIDE", "CONFLICTING_OVERLOADS", "EXTERNAL_DELEGATION", "NESTED_CLASS_IN_EXTERNAL_INTERFACE")

package react.materialui


import kotlin.js.*
import kotlin.js.Json
import org.khronos.webgl.*
import org.w3c.dom.*
import org.w3c.dom.events.*
import org.w3c.dom.parsing.*
import org.w3c.dom.svg.*
import org.w3c.dom.url.*
import org.w3c.fetch.*
import org.w3c.files.*
import org.w3c.notifications.*
import org.w3c.performance.*
import org.w3c.workers.*
import org.w3c.xhr.*
import react.RClass
import react.RProps
import react.ReactElement

@JsModule("@material-ui/core/StepLabel/StepLabel")
external val StepLabelImport: dynamic

external interface StepLabelProps : RProps {
    var active: Boolean? get() = definedExternally; set(value) = definedExternally
    var alternativeLabel: Boolean? get() = definedExternally; set(value) = definedExternally
    var children: ReactElement
    var completed: Boolean? get() = definedExternally; set(value) = definedExternally
    var disabled: Boolean? get() = definedExternally; set(value) = definedExternally
    var error: Boolean? get() = definedExternally; set(value) = definedExternally
    var icon: dynamic get() = definedExternally; set(value) = definedExternally
    var last: Boolean? get() = definedExternally; set(value) = definedExternally
    var optional: dynamic get() = definedExternally; set(value) = definedExternally
    var orientation: dynamic get() = definedExternally; set(value) = definedExternally
}
var StepLabel: RClass<StepLabelProps> = StepLabelImport.default

